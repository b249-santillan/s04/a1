//alert(`B249!`)

console.log(`s04- Nested Classes`);

class Student {
	constructor(name, email, grades) {
		this.name = name;
		this.email = email;
		this.gradeAve = undefined;
		this.passed = undefined;
		this.passedWithHonors = undefined;

		if(grades.length === 4){
		     if(grades.every(grade => grade >= 0 && grade <= 100)){
		         this.grades = grades;
		     } else {
		         this.grades = undefined;
		     }
		 } else {
		     this.grades = undefined;
		 }
	}

	login() {
		console.log(`${this.email} has logged in`);
		return this;
	}
	logout() {
		console.log(`${this.email} has logged out`);
		return this
	}
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		return this;
	}
	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		this.gradeAve = sum/4;
		return this;
	}
	willPass() {
		this.passed = this.computeAve().gradeAve >= 85 ? true : false;
		return this;
	}

	willPassWithHonors() {
		if (this.passed) {
		    if (this.gradeAve >= 90) {
		        this.passedWithHonors = true;
		    } else {
		        this.passedWithHonors = false;
		    }
		} else {
		    this.passedWithHonors = false;
		}
		return this;
	}
}


/* SECTION CLASS - class to represent a secion of students
*/
	class Section {
		// every section object will be instantiated with an empty array for its students
		constructor(name) {
			this.name = name;
			this.students = [];

			//for counting honor students
			this.honorStudents = 0;
			// for computing honors percentage
			this.honorsPercentage = undefined;
			this.sectionAve = undefined;
		}

		// method for adding a student to this section, this will take in the same arguments needed to instantiate a Student object
		addStudent (name, email, grades) {

			
			// a Student object will be instantiated before being pushed to the student property
			this.students.push(new Student(name, email, grades));

			
			// return the section object, allowing us ti chain this method.
			return this;
		}

		// count HONOR students method 
		countHonorStudents() {
			let count = 0;
			this.students.forEach(student => {

				if (student.computeAve().willPass().willPassWithHonors().passedWithHonors)
					{count ++}
			})
			this.honorStudents = count;
			return this;

		}

		// mini-activity 2
		computeHonorsPercentage() {
			this.honorsPercentage = (this.honorStudents/ this.students.length)*100
			return this;
		}

		//for batch ave
		computeSectionAve() {
			let totalGrade= 0
			this.students.forEach(student => 
			{
				totalGrade += student.gradeAve
			})
			this.sectionAve = (totalGrade/ this.students.length);
			return this
		}


	}

	// const section1A = new Section ('section1A');
	// console.log(section1A);


	/*
		Mini-activity:
			invoke the addStudent() method of the section1A while passing in the students' name, email, and grades as our arguments.

			name: 'John',
			email: 'john@mail.com',
			grades: [89, 84, 78, 88]
	*/

	// section1A.addStudent("John", "john@mail.com", [89, 84, 78, 88])
	// section1A.addStudent("Jane", "jane@mail.com", [78, 82, 79, 85])
	// section1A.addStudent("Joe", "joe@mail.com", [92, 89, 91, 96])
	// section1A.addStudent("Jessie", "jessie@mail.com", [97, 95, 92, 93])

	// console.log(section1A)

	// console.log(section1A.students[0])

	// Can we still invoke a particular student property and methods via dot notation? - YES: series of dot notation

	// console.log(section1A.students[0].computeAve())

	// Use the count honor students method
	// console.log(section1A.countHonorStudents())

	/*
		Mini-activity 2
			create a nethod that is a replica of our computeHonorPercentage()

				- create a property honorsPercentage = undefined
				- create computeHonorsPercentage() wc wil compute the total number of honor students / total num of students * 100
				- resulting value will be assigned to the property honorsPercentage
				- return the object
	*/

	//
	// console.log(`HONORS PERCENTAGE OF CLASS`)
	// console.log(`${section1A.countHonorStudents().computeHonorsPercentage().honorsPercentage}%`)


/* ACTIVITY! */

	/* FUNCTION CODING:

		1. Define a Grade class whose constructor will accept a number
		argument to serve as its grade level. It will have the following
		properties:
		● level initialized to passed in number argument
		● sections initialized to an empty array
		● totalStudents initialized to zero
		● totalHonorStudents initialized to zero
		● batchAveGrade set to undefined
		● batchMinGrade set to undefined
		● batchMaxGrade set to undefined

		
	*/

	//GRADE CLASS

	class Grade {
		constructor(level) {
			if (typeof level === "number")
			{this.level = level}
			else
			{console.log(`must input a number as level`)}
			this.sections = [];
			this.totalStudents = 0;
			this.totalHonorStudents = 0;
			this.batchAveGrade = undefined;
			this.batchMinGrade= undefined;
			this.batchMaxGrade = undefined
		}

		/*
			2. Define an addSection() method that will take in a string argument to
			instantiate a new Section object and push it into the sections array
			property.
		*/

		addSection(sectionName){
			if (typeof sectionName === "string")
			{
				this.sections.push(new Section (sectionName))
				//console.log(grade1);
				return this;

			}
			else {console.log(`section must be of data type: string`)}

			
		}

		/*
			4. Define a countStudents() method that will iterate over every section in
			the grade level, incrementing the totalStudents property of the grade level
			object for every student found in every section.
		*/
		countStudents () {
			let count = 0;
			this.sections.forEach(section => count += section.students.length )
			
			this.totalStudents = count;
			return this;

		}

		/*
			5. Define a countHonorStudents() method that will perform similarly to
			countStudents() except that it will only consider honor students when
			incrementing the totalHonorStudents property.
		*/

		countHonorStudents () {
			let honorCount = 0;
			this.sections.forEach(section => section.countHonorStudents())
			this.sections.forEach(section => honorCount += section.honorStudents)
			
			this.totalHonorStudents = honorCount;
			return this;
		}

		computeBatchAve() {
			let batchTotal = 0
			this.sections.forEach(section => section.computeSectionAve())
			this.sections.forEach(section => batchTotal += section.sectionAve)
			this.batchAveGrade = (batchTotal/this.sections.length)
			return this;
		}





		
	}

	// #1 grade instantiation
	const grade1 = new Grade (1);
	//console.log(grade1);

	// #2 add section
	//grade1.addSection("section1A")
	// console.log(grade1.sections);
	//console.log(grade1);
	//save sections of this grade level as constants
	
	// console.log(grade1);

	grade1.addSection("section1A")
	grade1.addSection("section1B")
	grade1.addSection("section1C")
	grade1.addSection("section1D")
	console.log(grade1);




	/*
		3. Write a statement that will add a student to section1A. The student is
		named John, with email john@mail.com, and grades of 89, 84, 78, and 88.

		input all students
	*/
	const section1A = grade1.sections.find(section => section.name === "section1A");
	const section1B = grade1.sections.find(section => section.name === "section1B");
	const section1C = grade1.sections.find(section => section.name === "section1C");
	const section1D = grade1.sections.find(section => section.name === "section1D");

	section1A.addStudent('John', 'john@mail.com', [89, 84, 78, 88]);
	section1A.addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85]);
	section1A.addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93]);
	section1A.addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

	section1B.addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]);
	section1B.addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]);
	section1B.addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]);
	section1B.addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

	section1C.addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]);
	section1C.addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]);
	section1C.addStudent('Love', 'love@mail.com', [91, 87, 90, 88]);
	section1C.addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

	section1D.addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]);
	section1D.addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]);
	section1D.addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]);
	section1D.addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);



	// #4 COUNT STUDENTS
	//grade1.countStudents()
	console.log(`Count total students`)
	console.log(grade1.countStudents())


	// #5 COUNT HONOR STRUDENTS
	//grade1.countHonorStudents()
	console.log(`Count total honor students`)
	console.log(grade1.countHonorStudents())

	/*
		6. Define a computeBatchAve() method that will get the average of all the
		students' grade averages and divide it by the total number of students in
		the grade level. The batchAveGrade property will be updated with the
		result of this operation.
	*/

	// #5 COMPUTE BATCH AVERAGE
	console.log(`Compute Batch Average`)
	console.log(grade1.computeBatchAve())